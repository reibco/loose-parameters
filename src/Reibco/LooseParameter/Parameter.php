<?php
namespace Reibco\LooseParameter;

class Parameter
{
    protected $name;
    protected $required;
    protected $type;
    protected $default;
    protected $value;

    /**
     * Construct.
     */
    public function __construct($name, $required, $type, $default, $value)
    {
        $this->name = $name;
        $this->required = !!$required;
        $this->type = $type;
        $this->default = $default;
        $this->value = $value;

        // if the parameter is optional, assign the default value
        if ($this->optional()) {
            $this->assign($this->default);
        }
    }

    /**
     * Return the name of the parameter.
     *
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Return the value of the parameter.
     *
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * Set the value of the parameter.
     *
     * @param mixed $value
     * @return Reibco\LooseParameter\Parameter
     */
    public function assign($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Check if the parameter is required.
     *
     * @return boolean
     */
    public function required()
    {
        return $this->required;
    }

    /**
     * Check if the parameter is optional.
     *
     * @return boolean
     */
    public function optional()
    {
        return !$this->required();
    }

    /**
     * Check if a value has been assigned to the parameter.
     *
     * @return boolean
     */
    public function assigned()
    {
        return isset($this->value);
    }

    /**
     * Check if a value has not been assigned to the parameter.
     *
     * @return boolean
     */
    public function unassigned()
    {
        return !$this->assigned();
    }
}
