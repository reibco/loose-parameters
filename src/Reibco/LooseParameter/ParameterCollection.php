<?php
namespace Reibco\LooseParameter;

use Illuminate\Support\Collection;

class ParameterCollection extends Collection
{
    // currently completely implemented via Illuminate\Support\Collection
}
