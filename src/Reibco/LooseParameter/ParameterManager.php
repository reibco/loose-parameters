<?php
namespace Reibco\LooseParameter;

use Illuminate\Translation\Translator;
use InvalidArgumentException;

class ParameterManager
{
    protected $parameters;
    protected $translator;

    /**
     * Constructor
     */
    public function __construct(ParameterCollection $parameters, Translator $translator)
    {
        $this->parameters = $parameters;
        $this->translator = $translator;
    }

    /**
     * Register a new parameter with the manager.
     *
     * @param string $name
     * @param boolean $required
     * @param string $type
     * @param mixed $default
     * @param mixed $value
     * @return Reibco\LooseParameter\ParameterManager
     */
    public function registerParameter($name, $required, $type = null, $default = null, $value = null)
    {
        $parameter = new Parameter($name, $required, $type, $default, $value);
        $this->putParameter($name, $parameter);

        return $this;
    }

    /**
     * Assign data to a parameter or an array of parameters.
     *
     * @param string|array $name
     * @param mixed $value
     * @return Reibco\LooseParameter\ParameterManager
     */
    public function assignData($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                $this->assignData($key, $value);
            }
        } else {
            $parameter = $this->getParameter($name);
            $parameter->assign($value);
            $this->putParameter($name, $parameter);
        }

        return $this;
    }

    /**
     * Get the value of a parameter.
     *
     * @param string $name
     * @return mixed
     */
    public function getValue($name)
    {
        $parameter = $this->getParameter($name);
        return $parameter->value();
    }

    /**
     * Validate the registered parameters are set correctly.
     *
     * @throws InvalidArgumentException
     * @return Reibco\LooseParameter\ParameterManager
     */
    public function validate()
    {
        foreach ($this->allParameters() as $parameter) {
            if ($parameter->required() && $parameter->unassigned()) {
                throw new InvalidArgumentException($this->translator->trans(
                    'loose-parameter::parameters.missingreq',
                    ['name' => $parameter->name()]
                ));
            }
        }

        return $this;
    }

    /**
     * Get an array of all the parameters
     *
     * @return array
     */
    protected function allParameters()
    {
        return $this->parameters->all();
    }

    /**
     * Get a specific parameter by name
     *
     * @param string $name
     * @return Reibco\LooseParameter\Parameter
     */
    protected function getParameter($name)
    {
        return $this->parameters->get($name);
    }

    /**
     * Add or update a parameter in the collection by name.
     *
     * @param string $name
     * @param Reibco\LooseParameter\Parameter $parameter
     * @return Reibco\LooseParameter\ParameterManager
     */
    protected function putParameter($name, $parameter)
    {
        $this->parameters->put($name, $parameter);

        return $this;
    }
}
