<?php
return array(

    /**
     * --------------------------------------------------------------------------
     * Parameter Messages
     * --------------------------------------------------------------------------
     */

    'missingreq' => "Parameter :name is required but is not assigned."
);
